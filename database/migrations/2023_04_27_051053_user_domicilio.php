<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
      Schema::create('user_domicilio', function (Blueprint $table) {
          $table->id();
          $table->foreignId('user_id')->index();
          $table->string('domicilio',150);
          $table->string('numero_exterior',8);
          $table->string('colonia',150);
          $table->string('cp',5);
          $table->string('ciudad',150);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
